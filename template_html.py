
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Author:                     Hendrik Schutter, mail@hendrikschutter.com
    Date of creation:           2022/07/20
    Date of last modification:  2022/07/20
"""

def html_comparison_head():
   return '''
<!DOCTYPE html><html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="shortcut icon" href="../data/favicon.ico">
      <link rel="icon" sizes="192x192" href="../data/icon.png">
      <title>eBay competitor price compare</title>
      <link rel="stylesheet" href="../css/w3.css">
      <style>
         body {
         background-color: #000000;
         background-repeat: no-repeat;
         background-attachment: fixed;
         background-size: cover;
         }
         .holster {
         display: flex;
         align-items: center;
         justify-content: space-between;
         flex-flow: column nowrap;
         font-family: monospace;
         }
         .container {
         display: flex;
         overflow: auto;
         outline: 1px dashed lightgray;
         flex: none;
         }
         .container.y {
         width: 100%;
         height: 500px;
         flex-flow: column nowrap;
         }
         .y.mandatory-scroll-snapping {
         scroll-snap-type: y mandatory;
         }
         .y.proximity-scroll-snapping {
         scroll-snap-type: y proximity;
         }
         .container > .result_scroll_element {
         text-align: center;
         scroll-snap-align: center;
         flex: none;
         }
         .y.container > .result_scroll_element {
         /*line-height: 256px;*/
         font-size: 128px;
         width: 100%;
         height: 100%;
         }
         img{
         max-width:600px;
         max-width:600px;
         }
      </style>
   </head>
'''

def html_comparison_navigation(counter): 

   back_link = str((str(counter-1)+".html") if ((counter-1) > 0) else "#")
   current = str(counter)
   current_link = str(counter)+".html"
   next_link = str((str(counter+1)+".html")) #TODO: test if last one and replace with '#'

   return '''    
<div class="w3-cell-row">
         <div class="w3-container w3-dark-gray w3-cell">
            <a class="w3-hover" style="text-decoration: none" href="''' + back_link + '''">
               <h1>&larr; Back</h1>
            </a>
         </div>
         <div class="w3-container w3-dark-gray w3-cell w3-center">
            <a class="w3-hover" style="text-decoration: none" href="''' + current_link + '''">
               <h1>Compare #''' + current + '''</h1>
            </a>
         </div>
         <div class="w3-container w3-dark-gray w3-cell">
            <a class="w3-hover" style="text-decoration: none; text-align: right;" href="''' + next_link + '''">
               <h1>&rarr; Next</h1>
            </a>
         </div>
      </div>
'''

def html_comparison_seller_listing(seller_listing):
   return '''
      <div>
      <a class="w3-hover" style="text-decoration: none" href="''' + seller_listing['url'] + '''" target="_blank">
         <div class="w3-container">
            <div class="w3-panel w3-green">
               <div class="w3-container w3-cell">
                  <img src="''' + seller_listing['image'] + '''" class="w3-card-4" alt="image not found" style="float:left">
               </div>
               <div class="w3-container w3-cell">
                  <h1 class="w3-monospace w3-xxlarge">''' + seller_listing['title'] + '''</h1>
                  <h2 class="w3-monospace w3-jumbo">''' + str(seller_listing['price']) + ''' €</h2>
               </div>
            </div>
      </a>
      </div>
      '''

def html_comparison_competitor_list_header():
   return '''
            <h3 class="w3-text-white">Competitor results:</h3>
      <div class="holster">
      <div class="container y mandatory-scroll-snapping" dir="ltr">
      '''

def html_comparison_competitor_listing(competitor_listing, competitor_listing_counter):
   return '''
      <div class="result_scroll_element w3-text-white">
         <a class="w3-hover" style="text-decoration: none" href="''' + competitor_listing['url'] + '''" target="_blank">
            <div class="w3-container">
               <div class="w3-panel w3-indigo">
                  <h6 class="w3-text-white">#''' + str(competitor_listing_counter) + '''</h6>
                  <div class="w3-container w3-cell">
                     <img src="''' + competitor_listing['image'] + '''" class="w3-card-4" alt="image not found" style="float:left">
                  </div>
                  <div class="w3-container w3-cell">
                     <h1 class="w3-monospace w3-xxlarge">''' + competitor_listing['title'] + '''</h1>
                     <h2 class="w3-monospace w3-jumbo">''' + str(competitor_listing['price']) + ''' €</h2>
                  </div>
               </div>
         </a>
         </div>
      </div>
      '''

def html_comparison_trailer():
   return '''
      </div>
   </body>
</html>
'''

def html_startpage_head():
   return '''
<!DOCTYPE html>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="shortcut icon" href="./data/favicon.ico">
      <link rel="icon" sizes="192x192" href="./data/icon.png">
      <title>eBay competitor price compare</title>
      <link rel="stylesheet" href="./css/w3.css">
      <style>
        body {
          background-color: #000000;
          background-repeat: no-repeat;
          background-attachment: fixed;
          background-size: cover;
        }
    </style>
   </head>
   '''

def html_startpage_info(seller_listings_count, cheaper_listings_count, compare_time, export_time, date):



   return '''
     <div class="w3-container w3-content w3-center" style="margin-top:1%">
         <h1 class="w3-wide w3-text-white">eBay competitor price compare</h1>
         <div class="w3-panel w3-card-4 w3-dark-gray w3-padding">
            <div class="w3-left-align">
            <h3>Seller info</h3>
            <p>Number of seller listings: ''' + str(seller_listings_count) + '''</p>
            <p>Possibly cheaper listings: ''' + str(cheaper_listings_count) + '''</p>
            <h3>Runtime info</h3>      
            <p>Compare time needed: ''' + str(compare_time) + '''</p>       
            <p>Export time needed: ''' + str(export_time) + '''</p>
            <p>Date of creation: ''' + str(date) + '''</p>     
         </div>
         <div class="w3-center-align">
            <form action="compare/1.html"+">
               <input type="submit" value="Start manual comparing" />
           </form>
         </div>
         </div>
         '''

def html_startpage_trailer():
   return  '''
   <div class="w3-container w3-content w3-center" style="max-width:600px;">
         <div class="w3-panel w3-card w3-dark-gray  w3-padding">
            <div class="w3-center-align">
                  <a class="w3-hover" style="text-decoration: none" href="mailto:mail@hendrikschutter.com">
               <h6>mail@hendrikschutter.com</h6></a>               
                  <a class="w3-hover" style="text-decoration: none" href="https://git.mosad.xyz/localhorst/eBayCompetitorPriceCompare">
               <h6>https://git.mosad.xyz/localhorst/eBayCompetitorPriceCompare</h6></a>
            </div>
         </div>
      </div>
      <footer class="w3-container w3-center">Last update: 2022/03/30/<footer>
   </body>
</html>
   '''




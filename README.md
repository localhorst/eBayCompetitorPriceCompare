# eBayCompetitorPriceCompare

scrape ebay(.de) for competitors listings and compare the price to own ebay shop

## Requirements

```
pip install tinydb tqdm 
```

## Usage

### Scrape listings 
Scrape all listings from seller's shop with `python search_seller.py`
This will create `seller_db.json` with all found listings. 
Delete this file before next run manually.

### Compare listings
Compare and export all found listings with `python compare.py`
This will take a while and exports the static html.

## Example export
See `example_export.zip` for an example.

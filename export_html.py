#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Author:                     Hendrik Schutter, mail@hendrikschutter.com
    Date of creation:           2022/07/20
    Date of last modification:  2022/07/20
"""

from datetime import datetime
import os
import template_html as thtml
import shutil


class exporter:
    export_dir = ""
    tsStart = 0
    counter = 0

    def __init__(self, path):
        self.export_dir = path
        self.tsStart = datetime.now()  # set start time for exporting
        try:
            os.mkdir(self.export_dir)
        except FileExistsError:
            pass
        try:
            os.mkdir(os.path.join(self.export_dir, "compare/"))
        except FileExistsError:
            pass

        self.copy_static_export()

    def copy_static_export(self):
        try:
            os.mkdir(os.path.join(self.export_dir, "css/"))
        except FileExistsError:
            pass
        try:
            os.mkdir(os.path.join(self.export_dir, "data/"))
        except FileExistsError:
            pass

        shutil.copy(
            "./html/css/w3.css", os.path.join(self.export_dir, "css/", "w3.css")
        )
        shutil.copy(
            "./html/data/favicon.ico",
            os.path.join(self.export_dir, "data/", "favicon.ico"),
        )
        shutil.copy(
            "./html/data/icon.png", os.path.join(self.export_dir, "data/", "icon.png")
        )

    def export_comparison(self, seller_listing, competitor_listings):
        self.counter += 1
        f = open(
            os.path.join(self.export_dir, "compare/", str(self.counter) + ".html"), "a"
        )

        f.write(thtml.html_comparison_head())
        f.write("<body>")
        f.write(thtml.html_comparison_navigation(self.counter))
        f.write(thtml.html_comparison_seller_listing(seller_listing))
        f.write(thtml.html_comparison_competitor_list_header())
        competitor_listing_counter = 0
        for competitor_listing in competitor_listings:
            competitor_listing_counter += 1
            f.write(
                thtml.html_comparison_competitor_listing(
                    competitor_listing, competitor_listing_counter
                )
            )
        f.write(thtml.html_comparison_trailer())
        f.close()

    def export_startpage(
        self, seller_listings_count, cheaper_listings_count, compare_time, date
    ):

        duration_export = datetime.now() - self.tsStart
        hours, remainder = divmod(duration_export.total_seconds(), 3600)
        minutes, seconds = divmod(remainder, 60)
        duration_export = (
            str(hours) + "h " + str(minutes) + "m " + str(round(seconds, 2)) + "s"
        )

        f = open(os.path.join(self.export_dir, "index.html"), "a")
        f.write(thtml.html_startpage_head())
        f.write(
            thtml.html_startpage_info(
                seller_listings_count,
                cheaper_listings_count,
                compare_time,
                duration_export,
                date,
            )
        )
        f.write(thtml.html_startpage_trailer())
        f.close()


if __name__ == "__main__":

    seller_listing_dummy = {
        "title": "Seller Title",
        "price": float(42.42),
        "image": "https://i.ebayimg.com/images/g/7lAAAOSw~ixieBVP/s-l500.jpg",
        "url": "https://www.ebay.de/itm/165508291809",
    }

    competitor_listings_dummy = [
        {
            "title": "Competitor Title 01",
            "price": float(40.42),
            "image": "https://i.ebayimg.com/images/g/7lAAAOSw~ixieBVP/s-l500.jpg",
            "url": "https://www.ebay.de/itm/165508291809",
        },
        {
            "title": "Competitor Title 02",
            "price": float(41.42),
            "image": "https://i.ebayimg.com/images/g/7lAAAOSw~ixieBVP/s-l500.jpg",
            "url": "https://www.ebay.de/itm/165508291809",
        },
        {
            "title": "Competitor Title 03",
            "price": float(42.00),
            "image": "https://i.ebayimg.com/images/g/7lAAAOSw~ixieBVP/s-l500.jpg",
            "url": "https://www.ebay.de/itm/165508291809",
        },
    ]

    exp = exporter("./html_out/")
    exp.export_comparison(seller_listing_dummy, competitor_listings_dummy)

    exp.export_startpage(10, 2, 0, "d")
